<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('products', function (Blueprint $table) {
            /** define table's storage engine */
            //$table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('libelle')->unique();
            $table->string('id_categorie');
            $table->timestamps();
        });

        /** update table by adding new field or update column */
        /*
        Schema::table('users', function ($table) {
            $table->string('email');
        });
        */


        Schema::create('categorie', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lib_cat')->unique();
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /**  rename an existing database table */
        //Schema::rename($from, $to);

        /** drop an existing database table */
        //Schema::dropIfExists('users');
    }
}
